package com.chickenman.scify

import com.mojang.blaze3d.platform.InputUtil
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper
import org.lwjgl.glfw.GLFW
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.swing.text.JTextComponent.KeyBinding


object SciFyMain : ModInitializer {
    val LOGGER: Logger = LoggerFactory.getLogger("SciFy")
    val Mod_ID = "scify"

    override fun onInitialize(mod: ModContainer) {
        //LOGGER.info("Hello Quilt world from {}!", mod.metadata()?.name())
    }
}
